package ru.tsc.panteleev.tm;

import ru.tsc.panteleev.tm.api.ICommandRepository;
import ru.tsc.panteleev.tm.constant.ArgumentConst;
import ru.tsc.panteleev.tm.constant.TerminalConst;
import ru.tsc.panteleev.tm.model.Command;
import ru.tsc.panteleev.tm.repository.CommandRepository;
import ru.tsc.panteleev.tm.util.NumberUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) throws IOException {
        if (run(args))
            System.exit(0);
        showWelcome();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("Enter command:");
            String command = bufferedReader.readLine();
            runWithCommand(command);
        }
    }

    public static void runWithCommand(String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.INFO:
                showSystemInfo();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    public static void runWithArgument(String command) {
        switch (command) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            default:
                showErrorArgument(command);
                break;
        }
    }

    public static boolean run(String[] args) {
        if (args == null || args.length == 0) return false;
        final String command = args[0];
        if (command == null || command.isEmpty()) return false;
        runWithArgument(command);
        return true;
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showWelcome() {
        System.out.println("Welcome to Task Manager");
    }

    public static void showAbout() {
        System.out.println("Name: Sergey Panteleev");
        System.out.println("E-mail: spanteleev@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("1.8.0");
    }

    public static void showHelp() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            System.out.println(command);
        }
    }

    public static void showCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            final String name = command.getName();
            if (name != null && !name.isEmpty())
                System.out.println(name);
        }
    }

    public static void showArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            final String argument = command.getArgument();
            if (argument != null && !argument.isEmpty())
                System.out.println(argument);
        }
    }

    public static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (threads): " + availableProcessors);
        final long maxMemory = runtime.maxMemory();
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = isMemoryLimit ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.println("Maximum memory available: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        System.out.println("Total memory available: " + NumberUtil.formatBytes(totalMemory));
        final long freeMemory = runtime.freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void showErrorCommand(String command) {
        System.err.printf("Error! This command `%s` not supported... \n", command);
        showHelp();
    }

    public static void showErrorArgument(String command) {
        System.err.printf("Error! This argument `%s` not supported... \n", command);
        showHelp();
    }

}
